# Cypress Segmentation API test framework

This is a repo to use `Cypress` to run API integration tests for the Segmentation Service

## Libraries and plugins used

- [Cypress](https://www.cypress.io/)
- [cy-api](https://github.com/bahmutov/cy-api) plugin

## Pre-conditions:
The following are installed on your machine:
1) Node 
2) NPM
3) IntelliJ
4) Within IntelliJ, install the plug-in called "cypress support"

## How to run
1) Install the project dependencies
`npm i`

2) Install cypress globally as admin (by default, laptops issued by Caesars appear to require admin privileges for this)
`sudo npm install cypress -g`

(FOR STEPS 3 and 4, if the command fails, try replacing npm with npx)

3) Run the tests on your terminal:
`npm run cy:test`

4) OR Run the tests using the Cypress Test Runner (if this doesn't work, try the one above step #3):
`npm run cypress open`

5) Once the test runner has loaded, click on the spec file you wish to test.
