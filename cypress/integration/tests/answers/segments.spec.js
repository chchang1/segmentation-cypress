/// <reference types="cypress" />

const baseURL = "http://us-remorhaz.aws-us-east-1.usd.williamhill.plc";

describe('segments API', () => {
  it('retrieve the overall list of segments', () => {
    cy.request(baseURL + '/v1/segments')
      .its('headers')
      .its('content-type')
      .should('include', 'application/json');
  });

  it('has the correct status code', () => {
    cy.request(baseURL + '/v1/segments').its('status').should('be.equal', 200);
  });

  it('returns a size', () => {
    cy.request(baseURL + '/v1/segments').then((res) => {
      res.body.forEach((item) => expect(item.size).to.equal(0));
    });
  });
});
